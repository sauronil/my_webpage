# my_webpage

Create a web page using HTML and CSS. 

Resposive Webpage by adding media queries, taking standard resolution used in Bootstrap


## Contents

Web Page Contents

- Home Page
- Brief Introduction To Computational Thinking
- Biography on Linus Torvalds
- Reflection on CMT119 and how it has impacted me now and for the future

## Support

- dass19@cardiff.ac.uk
- sauronildas@protonmail.com

## Roadmap

Further integreate Javascript and PHP to make it dynamic and add backend tools such as MariaDB 

## License
Free to Use Code

## Project status

**ACTIVE** 
